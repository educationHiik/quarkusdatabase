
package ru.hiik;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

/**
 * Класс сервис, предназначенный для выполнения следующих операций:
 * 
 * 1. Размещение экземпляров класса Person  в базе данных 
 * 2. Получение полного списка экземпляров класса Person  
 * 
 * 
 * @author vaganovdv
 */
@ApplicationScoped
public class PersonService {

    private static final Logger log = Logger.getLogger(PersonService.class.getName());
    
    
    // Включение (внедрение) созданного на этапе старта сервера экземпляра класса  EntityManager
    @Inject
    EntityManager em; 
    
    // Строка запрроса для поиска всех записей о персонах
    // JPQL
    private final String findAllQuery ="Select person from " + Person.class.getSimpleName() + " person";
    
    /**
     * Функция 1: Сохранение записи о персоне в базе данных
     *
     * Метод выполняет запись и контрольное чтение
     *
     * @param person
     * @return
     */
    @Transactional
    public Person save(Person person) {
        Person savedPerson = null;
        if (person != null) {
            em.persist(person);  // Сохранение экземпляра Person  в базе данных
            // После выполнения метода persist  сущность получет уникальный идентификатор
            em.flush();          // Запись экземпляра в БД
            
            // Вызов метода контрольного чтения через поиск
            savedPerson = em.find(Person.class, person.getId());
            if (savedPerson != null) {
                String info = "Сохранена запись o персоне  [" + savedPerson.getSureName() + "] id = " + savedPerson.getId();
                log.info(info);
            } else {
                String error = "Ошибка сохранения записи о персоне: запись не сохранена в базе данных";
                log.log(Level.SEVERE, error);
            }
        } else {
            String error = "Ошибка сохранения записи о персоне: входной экземпляр персоны пустой";
            log.log(Level.SEVERE, error);
        }
        return savedPerson;
    }
    
    
  
    
     /**
     * Получение полного списка записей o персонах
     *
     * @return
     */
    @Transactional
    public List<Person> getAll() {        
        
        List<Person> personList = (List<Person>) em.createQuery(findAllQuery).getResultList();
        if (personList != null && !personList.isEmpty()) {
            String info = "Найдено [" + personList.size() + "] записей о персонах";
            
            log.info(info);            
        } else {
            log.log(Level.WARNING,"Список персон пустой");
        }
        return personList;
    }
    
    
    

}
