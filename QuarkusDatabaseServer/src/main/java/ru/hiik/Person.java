package ru.hiik;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Example JPA entity.
 *
 * To use it, get access to a JPA EntityManager via injection.
 *
 * {@code
 *     @Inject
 *     EntityManager em;
 *
 *     public void doSomething() {
 *         MyEntity entity1 = new MyEntity();
 *         entity1.setField("field-1");
 *         em.persist(entity1);
 *
 *         List<MyEntity> entities = em.createQuery("from MyEntity", MyEntity.class).getResultList();
 *     }
 * }
 */


@Entity                     // Аннотация - признак того, что сущность Person должна храниться в БД
@Table(name="persons")      // Аннотация - имени таблицы, в которой хранятся экземпляры Person

public class Person {
    
    @Id                 // Аннотация - признак номера записи в таблице Posgres 
    @GeneratedValue     // Аннотация - признак автоматической генерации номера записи в таблице Posgres
    private Long id;    // Идентификатор записи в таблице Posgres (присваивается не программистом, а базой данных автоматически)
    
    private String firstName;   // имя персоны
    private String sureName;    // фамилия персоны
    private String middleName;  // отчество персоны

  
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSureName() {
        return sureName;
    }

    public void setSureName(String sureName) {
        this.sureName = sureName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

  
}
