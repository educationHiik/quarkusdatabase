package ru.hiik;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import java.util.List;
import java.util.logging.Logger;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

/**
 *
 * @author developer
 */
@ApplicationScoped
public class StartStopService {

    private static final Logger log = Logger.getLogger(StartStopService.class.getName());

    @Inject
    PersonService personService;

    
    /**
     * Метод автоматически вызывается при старте сервера quarkus
     * 
     * @param ev 
     */
    void start(@Observes StartupEvent ev) {
        log.info("Старт сервера... ");
        this.initDatabase();

        log.info("Сервер стартовал");
    }

    /**
     * Метод автоматически вызывается при старте сервера quarkus
     * 
     * @param ev 
     */
    void stop(@Observes ShutdownEvent ev) {
        log.info("Остановка сервера... ");

        log.info("Сервер остановлен");
    }

    
    
    private void initDatabase() {
  
        log.info("Добавление в базу данных персоны...");
        Person person1 = new Person();
        person1.setFirstName("Иван");
        person1.setMiddleName("Иванович");
        person1.setSureName("Иванов");

        personService.save(person1);

        
        List<Person> personList = personService.getAll();
        log.info("В базе данных содержится список ["+personList.size()+"] персон");
        
    }

}
