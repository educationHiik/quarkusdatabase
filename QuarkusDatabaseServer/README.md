# quarkus-database-server Project

Проект для демонстрации работы с базой данных Postgres

### Установка среды виртулизации Docker в Linux

Инструкция по установке Docker размещена по адресу:

[Docker установка на Linux ](https://docs.docker.com/engine/install/ubuntu/)


## Установка базы данных Postgres в среде виртуализации Docker 

```shell script
docker run -d \
    --name postgres-hiik \
    -p 5432:5432 \
    -e POSTGRES_USER=postgres \
    -e POSTGRES_PASSWORD=postgres \
    postgres:14.2
```





## Запуск приложения с командной строки в режиме разработки


```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus в настоящее время имеет графический интерфейс ползователя, доступный в режиме разработки по адресу  http://localhost:8080/q/dev/.

## Packaging and running the application

The application can be packaged using:
```shell script
./mvnw package
```
It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory.
Be aware that it’s not an _über-jar_ as the dependencies are copied into the `target/quarkus-app/lib/` directory.

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.


